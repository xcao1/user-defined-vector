#include <iostream>
#include <string>
#include <vector>
#include "MyVector.h"
using namespace std;
int main(void)
{
	try{
		
		/*myvector<string> mArray;
		cout<<"size : "<<mArray.size()<<endl;
		cout<<"capacity : "<<mArray.capacity()<<endl;
		mArray.resize(INT_MAX-4);
		mArray.push_back("end");
		cout<<"size : "<<mArray.size()<<endl;
		cout<<"capacity : "<<mArray.capacity()<<endl;*/
		/*int **a = new int*[4];
		for( int i = 0; i != 4; i ++ )
		{
			a[i] = new int [4];
		}
		for( int i = 0; i != 4; i ++ )
			for( int j = 0; j != 4; j ++ )
				a[i][j] = 0;
		a[1][0] = 1;
		a[2][0] = 2;
		a[3][0] = 3;
		for( int i = 0; i != 4; i ++ ){
			cout<<i<<" ";
			for( int j = 0; j != 4; j ++ ){
				std::cout<<a[i][j];
			}
			endl(cout);
		}
		cout<<"=========================="<<endl;
		myvector< int *> mArray( a, 4 );
		mArray[2][2] = 9;
		for( int i = 0; i != 4; i ++ ){
			cout<<i<<" ";
			for( int j = 0; j != 4; j ++ ){
				std::cout<<mArray[i][j];
			}
			endl(cout);
		}
		std::cout<<"size : "<<mArray.size()<<endl;
		cout<<"capacity : "<<mArray.capacity()<<endl;*/

		
	/*char arrayOne[12] = {' ','A','?','p','O','K','6',7,'8','9','10','11'};
	myvector<int> mArray(10);*/
	string a[2] = {"First string","Second string"};
	myvector<string> mArray2(a,2);
	myvector<string> mArray(mArray2);
	for(int i = 0; i != mArray.size(); i ++)
	{
		cout<<i<<"      ";
		cout<<mArray[i]<<endl;
	}
	cout<<"Array Size : "<<mArray.size()<<endl;
	cout<<"Capacity   : "<<mArray.capacity()<<endl;
	if(!mArray.empty())
		cout<<"Not Empty"<<endl;
	else
		cout<<"Empty"<<endl;
	cout<<"=================================="<<endl;
	// resize
	mArray.resize(6);
	for(int i = 0; i != mArray.size(); i ++)
	{
		cout<<i<<"      ";
		cout<<mArray[i]<<endl;
	}
	cout<<"Array Size : "<<mArray.size()<<endl;
	cout<<"Capacity   : "<<mArray.capacity()<<endl;
	cout<<"=================================="<<endl;
	mArray.at(0) = "100";
	mArray[1] = string("-99999");
	mArray.assign(2,string("222"));
	mArray.push_back(string("2012"));
	//string num = mArray.pop_back();
	//cout<<"number deleted : "<<num<<endl;
	mArray.insert(0,string("2007"));
	mArray.insert(0,string("2006"));
	mArray.insert(6,string("6"));
	mArray.assign(8,"8");
	//mArray[9] = "9";
	mArray.insert(mArray.size(),"End");
	//mArray.insert(100,"100999");
	//mArray.erase(mArray.size()-1);
	//mArray.erase(0,5);
	for(int i = 0; i != mArray.size(); i ++)
	{
		cout<<i<<"      ";
		cout<<mArray[i]<<endl;
	}
	cout<<"Array Size : "<<mArray.size()<<endl;
	cout<<"Capacity   : "<<mArray.capacity()<<endl;
	
	/*cout<<"=================================="<<endl;
	mArray.reserve(mArray.size());
	for(int i = 0; i != mArray.size(); i ++)
	{
		cout<<i<<"      ";
		cout<<mArray[i]<<endl;
	}
	cout<<"Array Size : "<<mArray.size()<<endl;
	cout<<"Capacity   : "<<mArray.capacity()<<endl;*/

	/*cout<<"============================"<<endl;
	mArray.erase(1,4);
	for(int i = 0; i != mArray.size(); i ++)
	{
		cout<<i<<"      ";
		cout<<mArray[i]<<endl;
	}
	cout<<"Array Size : "<<mArray.size()<<endl;
	cout<<"Capacity   : "<<mArray.capacity()<<endl;
	
	mArray.reserve(28);
	cout<<"Capacity   : "<<mArray.capacity()<<endl;
	
	
	cout<<"============================"<<endl;
	mArray.clear();
	for(int i = 0; i != mArray.size(); i ++)
	{
		cout<<i<<"      ";
		cout<<mArray[i]<<endl;
	}
	cout<<"Array Size : "<<mArray.size()<<endl;
	cout<<"Capacity   : "<<mArray.capacity()<<endl;
*/
	}catch(const char* msg){
		cout<<msg;
	}catch(out_of_range error){
		cout<<error.what();
	}catch(overflow_error overflow){
		cout<<overflow.what();
	}


















	/*myvector<int> mArray_1(10);
	for(int i = 0; i != mArray_1.mSize; i ++)
	{
		int num = mArray_1[i];
		std::cout<<num<<" ";
	}
	
	myvector<char> mArray_2(10);
	for(int i = 0; i != mArray_1.mSize; i ++)
	{
		char ch = mArray_2[i];
		std::cout<<ch<<" ";
		
	}


	myvector<int> mArray_3(mArray_1);
	for(int i = 0; i != mArray_3.mSize; i ++)
	{
		int num = mArray_3[i];
		std::cout<<num<<" ";
	}

	myvector<double> mArray_4(15);
	for(int i = 0; i != mArray_4.mSize; i ++)
	{
		double num = mArray_4[i];
		std::cout<<num<<" ";
	}

	std::cout<<"\n";

	std::string stringArray[10];
	for( int i = 0; i != 10; i ++ )
	{
		stringArray[i] = "this is a string";
	}

	myvector<std::string> mArray_5( stringArray, 10 );

	mArray_5[9] = "this is the end";

	for( int i = 0; i != 10; i ++ )
	{
		std::cout<< mArray_5[i]<<" ";
	}
	
	int **a = new int*[4];
	for( int i = 0; i != 4; i ++ )
	{
		a[i] = new int [4];
	}
	for( int i = 0; i != 4; i ++ )
		for( int j = 0; j != 4; j ++ )
			a[i][j] = 0;
	a[1][0] = 1;
	a[2][0] = 2;
	a[3][0] = 3;


	myvector< int *> mArray_6( a, 4 );

	for( int i = 0; i != 4; i ++ )
		for( int j = 0; j != 4; j ++ )
			std::cout<<a[i][j]<<" ";
	for( int i = 0; i != 4; i ++ )
		for( int j = 0; j != 4; j ++ )
			std::cout<<mArray_6[i][j]<<" ";
	std::cout<<"size : "<<mArray_6.size();*/


	string exit;
	std::cin>>exit;

	return 0;
}