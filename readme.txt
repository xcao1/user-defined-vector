    User-Defined Vector Assignment

 As introduced in class, a vector class is C++'s answer to the limitations of arrays. As compared with arrays, vectors protect the user from accessing out-of-bounds ranges, they resize dynamically on demand, and they know and can report their length.

This assignment will test your ability to create a "class library." This means that, while you may create a main function for testing your own code, the main function may not be submitted. Your code will be tested against a different main function. As a result, it is very important that you match class and function names to what we specify.

This assignment will be due on 12/19. However, no resubmission will be possible unless you submit by 12/12.

Your class must be named myvector and take one template type argument. Your class must have at least the following public members, which shall function as described:

int size() -- return the current length of the vector
int capacity() -- return the current capacity of the vector
bool empty() -- return whether or not the vector is empty
void resize(int) -- resize the vector (change length) to the specified size, changing capacity as needed
void reserve(int) -- change the vector capacity to the specified size, or give an error if capacity would get smaller than length

You must also implement the [] operator, which should return a member of the vector at the index specified. An error should be thrown if the index is out of range. A function named at should also exist and perform the exact same functionality. Thus I should be able to do:

myvector x(5);
x[0] = 10;

void assign(int, T) -- assign the second argument as the value of the vector at the position of the first argument
void push_back(T) -- increase length by 1 and add the argument as the value at the end
T pop_back() -- decrease length by 1, returning the value removed from the vector
void insert(int, T) -- increase length by 1, put the value specified in the second argument at index specified by first argument and shift all values to the right in the vector down by one index
void erase(int) -- decrease length by 1, remove the item at the specified index and shift all other items left to eliminate the "hole"
void erase(int, int) -- same as erase(int), but removes all elements between the indexes specified by the first and second argument
void clear() -- remove all elements from the list

You also need constructors that take the following arguments:
int -- initial length of the vector
another vector of the same type (as a const reference) -- copy the contents of the vector
an array of the same type -- copy contents of array into the vector

Any destructor is added only if you feel it is necessary.

Extra Credit Opportunity

You can earn 15 points extra credit by detecting all overflow conditions, and throwing an overflow_error rather than crashing. Make sure you put in your readme file that you attempted this bonus or we will not test your assignment to see if it works.